import React from 'react'; //ES6 syntax
import ReactDOM from 'react-dom';

import HomeView from './HomeView';
import SettingsView from './SettingsView';

ReactDOM.render(
  //<h1>Hello, World!</h1>,
  <HomeView />,
  document.getElementById('root')
);