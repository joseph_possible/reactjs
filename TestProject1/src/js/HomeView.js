import React, {Component} from 'react';

import '../css/HomeView.css'; //https://webpack.github.io/docs/stylesheets.html

export default class HomeView extends Component {
	constructor(props) {
		super(props);
		console.log('HomeView constructor called');
	}
	_handleClick() {
		alert('test');
	}
	render() {
		return (
			<div className="home-view-box" onClick={this._handleClick.bind(this)}>
				<div className="home-view-inner">
					<h3>Home View</h3>
					<p>This is a custom component created by Joseph</p>
				</div>
			</div>
		);
	}
}